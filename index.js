import axios from "axios";

class Wrapper{
    constructor({ apiKey }){
        this.apiKey = apiKey;
        this.baseUrl = `http://ws.audioscrobbler.com/2.0`
        this.client = axios.create({
            baseURL: this.baseUrl,
            params: {
                api_key: this.apiKey,
                format: 'json'
            }
        })
    }

    /**
    * @param {string} user - Username
    * @returns {Promise<Object>} - Returns the user's profile
    */
    getUser = async (user) => {
        if(!user) throw new Error('User is required')
        return await(await this.sendReq({ method: 'user.getInfo', user })).user
    }

    /**
    * @param {string} user - Username
    * @returns {Promise<Object>} - Returns the current playing track
    */
    getCurrentPlaying = async ( user ) => {
        if(!user) throw new Error('User is required')
        return await(await this.sendReq({ method: 'user.getrecenttracks', user, limit: 1 })).recenttracks.track[0]
    }

    /**
     * @param {string} user - Username
     * @param {string} track - Track name
     * @param {string} artist - Artist name
     * @returns {Promise<Object>} - Returns the track's info
    */
    getTrackPlayCount = async (user, track, artist) => {
        if(!user) throw new Error('User is required')
        if(!track) throw new Error('Track is required')
        if(!artist) throw new Error('Artist is required')

        return await(await this.sendReq({ method: 'user.gettrackscrobbles', user, track, artist })).trackscrobbles['@attr'].total
    }  


    /**
     * @param {string} user - Username
     * @param {string} album - Album name
     * @param {string} artist - Artist name
     * @returns {Promise<Object>} - Returns the album's info
    */
    getAlbumPlayCount = async (user, album, artist) => {
        if(!user) throw new Error('User is required')
        if(!album) throw new Error('Album is required')
        if(!artist) throw new Error('Artist is required')

        return await(await this.sendReq({ method: 'album.getInfo', user, album, artist })).album.userplaycount
    }

    /**
     * @param {string} user - Username
     * @param {string} artist - Artist name
     * @returns {Promise<Object>} - Returns the artist's info
     */
    getAristPlayCount = async (user, artist) => {
        if(!user) throw new Error('User is required')
        if(!artist) throw new Error('Artist is required')

        return await(await this.sendReq({ method: 'artist.getInfo', user, artist })).artist.stats.userplaycount
    }   


    /**
     * @param {object} params - request params
     * @returns {Promise<Object>} - Returns the request response
    */
    sendReq = async (params) => { 
        try {
            const { data: res} = await this.client.get(`/?method=${params.method}&user=${params.user}&limit=${params.limit}&artist=${params.artist}&track=${params.track}&album=${params.album}`)
            return res
        } catch(error) {
            throw new Error(error)
        }
    }
}

export default Wrapper;